package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"sync"
)

func main() {
	db := &safeDatabase{data: map[string]dollars{"shoes": 50, "socks": 5}}
	mux := http.NewServeMux()
	mux.HandleFunc("/list", db.list)
	mux.HandleFunc("/price", db.price)
	mux.HandleFunc("/create", db.create)
	mux.HandleFunc("/update", db.update)
	mux.HandleFunc("/delete", db.delete)
	log.Fatal(http.ListenAndServe("localhost:8000", mux))
}

type dollars float32

func (d dollars) String() string { return fmt.Sprintf("$%.2f", d) }

type safeDatabase struct {
	mu   sync.RWMutex
	data map[string]dollars
}

func (db *safeDatabase) list(w http.ResponseWriter, req *http.Request) {
	db.mu.RLock()
	defer db.mu.RUnlock()

	for item, price := range db.data {
		fmt.Fprintf(w, "%s: %s\n", item, price)
	}
}

func (db *safeDatabase) price(w http.ResponseWriter, req *http.Request) {
	db.mu.RLock()
	defer db.mu.RUnlock()

	item := req.URL.Query().Get("item")
	if price, ok := db.data[item]; ok {
		fmt.Fprintf(w, "%s\n", price)
	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "no such item: %q\n", item)
	}
}

func (db *safeDatabase) create(w http.ResponseWriter, req *http.Request) {
	db.mu.Lock()
	defer db.mu.Unlock()

	item := req.URL.Query().Get("item")
	priceStr := req.URL.Query().Get("price")

	if item == "" || priceStr == "" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "both 'item' and 'price' parameters are required for create operation")
		return
	}

	price, err := strconv.ParseFloat(priceStr, 32)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "invalid price format")
		return
	}

	db.data[item] = dollars(price)
	fmt.Fprintf(w, "item %q created with price %s\n", item, dollars(price))
}

func (db *safeDatabase) update(w http.ResponseWriter, req *http.Request) {
	db.mu.Lock()
	defer db.mu.Unlock()

	item := req.URL.Query().Get("item")
	priceStr := req.URL.Query().Get("price")

	if item == "" || priceStr == "" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "both 'item' and 'price' parameters are required for update operation")
		return
	}

	price, err := strconv.ParseFloat(priceStr, 32)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "invalid price format")
		return
	}

	if _, ok := db.data[item]; ok {
		db.data[item] = dollars(price)
		fmt.Fprintf(w, "item %q updated with price %s\n", item, dollars(price))
	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "no such item: %q\n", item)
	}
}

func (db *safeDatabase) delete(w http.ResponseWriter, req *http.Request) {
	db.mu.Lock()
	defer db.mu.Unlock()

	item := req.URL.Query().Get("item")

	if item == "" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "'item' parameter is required for delete operation")
		return
	}

	if _, ok := db.data[item]; ok {
		delete(db.data, item)
		fmt.Fprintf(w, "item %q deleted\n", item)
	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "no such item: %q\n", item)
	}
}
